#!/usr/bin/python3
import sys
from parser import parse

def 	error_double(letter):
	print("%s is defined twice, but not with the same value " % letter)
	sys.exit(1)

def count_occurence(data, letter):
	verif = 0
	for i in data['rules']['third']:
		if letter in i:
			verif = verif + 1
	return verif

def check_neg(line, c, data):
	for i, j in enumerate(line):
		if j == '!' and line[i + 1] == c:
			if data['facts'][c] == True:
				error_double(c)
			return False
	if data['facts'][c] == False:
		error_double(c)
	return True

def check_conclusion(conclusion, rule):
	if '^' in conclusion or '|' in conclusion:
		print("OR or XOR in conclusion is not yet supported")
		sys.exit(1)
	elif rule == 1:
		print('Double inclusion is not yet supported')
		sys.exit(1)


def solve(share, data, verbose):
	verif = 0
	if verbose:
		print(share)
	for i, j in enumerate(data['rules']['third']):
		if share[0] in j:
			try:
				if eval(data['rules']['first'][i]) == True:
					verif = verif + 1
					check_conclusion(j, data['rules']['second'][i])
					data['facts'][share[0]] = check_neg(j, share[0], data)
					if verif == count_occurence(data, share[0]):
						share.remove(share[0])
						return
				else:
					for i in data['rules']['first'][i]:
						if i.isupper() and data['facts'][i] == None:
							if i in share:
								break
							share.insert(0, i)
							return
			except TypeError:
				for i in data['rules']['first'][i]:
						if i.isupper() and data['facts'][i] == None:
							if i in share:
								break
							share.insert(0, i)
							return
			except SyntaxError:
				print('The syntax is not correct')
				print(data['rules']['first'][i])
	if data['facts'][share[0]] == None:
		data['facts'][share[0]] = False
	share.remove(share[0])

def result(data, verbose):
	share = []
	while 1:
		for i in data['queries']:
			if i not in share:
				share.insert(0, i)
			solve(share, data, verbose)
		if not share:
			break
	for i in data['queries']:
		print('With %s as True, %s is %s' % (data['display_facts'][0], i, data['facts'][i]))

if __name__ == '__main__':
	try:
		verbose = False
		if sys.argv[1] == '-v':
			data = parse(sys.argv[2])
			verbose = True
		else:
			data = parse(sys.argv[1])
		result(data, verbose)
	except IndexError:
		print('Usage: ./expert_system [input file]')
