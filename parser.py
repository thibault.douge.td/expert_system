#!/usr/bin/python3
import sys
import string



def valid_fact(line, valid_character):
	for c in line:
		if c not in valid_character:
			print('Unauthorized char in line %s' % line)
			sys.exit(1)
	return True

def valid_rule(line, valid_character, signs):
	for c in range(len(line)):
		if line[c] not in valid_character and line[c] not in signs:
			print('Unauthorized char in rule %s' % line)
			sys.exit(1)

	return True


def valid_query(line, valid_character):
	for c in line:
		if c not in valid_character:
			print('unauthorized char')
			sys.exit(1)
	return True

def fill_facts(data, fact):
	for i in fact:
		data['facts'][i] = True


def clean_lines(lines):
	recup = []
	for line in lines:
		line = line.replace(' ', '')
		if not line.startswith('#'):
			if line.find('#') > 0:
				line = line[:line.find('#')]
			recup.append(line)
	return recup

def valid_conclusion(conclusion):
	if '|' in conclusion or '^' in conclusion:
		return False
	sign = 0
	letter = 0
	for i in conclusion:
		if i == '+':
			sign += 1
		elif i in string.ascii_uppercase:
			letter += 1
	if sign != letter - 1:
		return False
	return True

def understandable_rules(lines):

	# Fonction pour rendre les rules comprehensibles par eval()

	rules = {
		'first' : [],
		'second': [],
		'third': []
	}
	tmp = []
	for i, line in enumerate(lines):
		if '<' in line:
			rules['second'].append(1)
			tmp = line.split('<=>')
			rules['first'].append(tmp[0])
			rules['third'].append(tmp[1])
		else:
			tmp = line.split('=>')
			rules['first'].append(tmp[0])
			rules['third'].append(tmp[1])
			rules['second'].append(0)
	for i, j in enumerate(rules['first']):
		rules['first'][i] = rules['first'][i].replace('+', ' and ')
		rules['first'][i] = rules['first'][i].replace('!', ' not ')
		rules['first'][i] = rules['first'][i].replace('|', ' or ')
		for t in j:
			if t.isupper():
				rules['first'][i] = rules['first'][i].replace(t, "data['facts']['"+ t +"']")
	return rules

def queries_add(data, line):
	for i in line:
		data['queries'].append(i)


def parse(filename):
	share = []
	tmp_rules = []
	try:
		with open(filename) as file:
			lines = file.read().splitlines()
	except FileNotFoundError:
		print('File not found')
		sys.exit(1)

	data = {
		'rules': {
			'first': [],
			'second': [],
			'third': [],
		},
		'queries': [],
		'facts': {},
	}

	for i in string.ascii_uppercase:
		data['facts'][i] = None

	lines = clean_lines(lines)
	display_facts = []
	for line in lines:
		if line.startswith('='):
			if valid_fact(line[1:], string.ascii_uppercase):
				fill_facts(data, line[1:])
				display_facts.append(line[1:])
		elif line.startswith('?'):
			if valid_query(line[1:], string.ascii_uppercase):
				queries_add(data, line[1:])
		elif line:
			if valid_rule(line, string.ascii_uppercase, '+|^!()<=>'):
				tmp_rules.append(line)
	data['rules'] = understandable_rules(tmp_rules)
	data['display_facts'] = display_facts
	return data
